/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minwidth_2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 12:08:22 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 12:08:53 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_minwidth_for_dioux_wizout_zero(t_flags *src)
{
	ssize_t		len;
	char		*tmp;
	char		*res;
	ssize_t		i;

	len = src->min_width - ft_strlen(src->s_str);
	if (len > 0)
	{
		i = 0;
		tmp = ft_strnew(len);
		while (len > i)
			tmp[i++] = ' ';
		if (src->minus == 1)
			res = ft_strjoin(src->s_str, tmp);
		else
			res = ft_strjoin(tmp, src->s_str);
		ft_strdel(&tmp);
		ft_strdel(&src->s_str);
		src->s_str = res;
	}
}

void	ft_minwidth_for_oux_with_zero(t_flags *src)
{
	ssize_t		len;
	char		*tmp;
	char		*res;
	ssize_t		i;

	len = src->min_width - ft_strlen(src->s_str);
	if (len > 0)
	{
		i = 0;
		tmp = ft_strnew(len);
		while (len > i)
			tmp[i++] = '0';
		res = ft_strjoin(tmp, src->s_str);
		ft_strdel(&tmp);
		ft_strdel(&src->s_str);
		src->s_str = res;
	}
}

void	ft_add_prefix_ox(t_flags *src, int f, int i, char *res)
{
	if (f == 1)
	{
		(src->c == 'x' || src->c == 'p') ? (res = ft_strjoin("0x", src->s_str))
		: 0;
		(src->c == 'X') ? (res = ft_strjoin("0X", src->s_str)) : 0;
		(src->c == 'o') ? (res = ft_strjoin("0", src->s_str)) : 0;
		ft_strdel(&src->s_str);
		src->s_str = res;
		return ;
	}
	while (src->s_str[i] == '0')
		i++;
	if (src->c != 'o' && i >= 2)
		src->s_str[1] = (src->c == 'x' || src->c == 'p') ?
		'x' : 'X';
	if ((src->c == 'x' || src->c == 'X' || src->c == 'p')
		&& i == 1)
	{
		src->s_str[0] = (src->c == 'x' || src->c == 'p') ?
		'x' : 'X';
		res = ft_strjoin("0", src->s_str);
		ft_strdel(&src->s_str);
		src->s_str = res;
	}
}

void	ft_minwidth_for_cs(t_flags *src, int i)
{
	ssize_t		len;
	char		*tmp;
	char		*res;
	char		c;

	len = src->min_width - ft_strlen(src->s_str);
	if (src->c == 'c' && src->s_str[0] == '\0')
	{
		src->n_bytes++;
		len--;
	}
	if (len > 0)
	{
		c = (src->zero == 1 && src->minus != 1) ? '0' : ' ';
		tmp = ft_strnew(len);
		while (len > i)
			tmp[i++] = c;
		if (src->minus == 1)
			res = ft_strjoin(src->s_str, tmp);
		else
			res = ft_strjoin(tmp, src->s_str);
		ft_strdel(&tmp);
		ft_strdel(&src->s_str);
		src->s_str = res;
	}
}
