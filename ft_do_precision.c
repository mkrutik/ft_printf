/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_do_precision.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 10:32:57 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 13:05:46 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_precision_cs(t_flags *src)
{
	char	*res;

	if (src->precision == 0)
	{
		ft_bzero(src->s_str, ft_strlen(src->s_str));
		return ;
	}
	res = ft_strsub(src->s_str, 0, src->precision);
	ft_strdel(&src->s_str);
	src->s_str = res;
}

void	ft_precision_dioux(t_flags *src, int i)
{
	char		*res;
	size_t		index;
	size_t		index_t;
	size_t		f;

	index = (src->s_str[0] == '-') ? 1 : 0;
	index_t = (src->s_str[0] == '-') ? 1 : 0;
	f = (src->s_str[0] == '-' && i == 1) ? (src->precision + 1) :
		src->precision;
	res = ft_strnew(f);
	(src->s_str[0] == '-') ? (res[0] = '-') : 0;
	while (index < f)
	{
		if (index < (f - ft_strlen(src->s_str) + ((src->s_str[0] == '-')
			? 1 : 0)))
			res[index++] = '0';
		else
			res[index++] = src->s_str[index_t++];
	}
	ft_strdel(&src->s_str);
	src->s_str = res;
}

void	ft_do_precision(t_flags *src)
{
	int	str_len;

	if (src->s_str)
		str_len = ft_strlen(src->s_str);
	if (src->precision >= 0)
	{
		if (src->c != 'c' && src->c != 's' && ((src->precision >
			(int)ft_strlen(src->s_str)) || ((src->precision >=
			(int)ft_strlen(src->s_str)) && src->s_str[0] == '-')))
			ft_precision_dioux(src, 1);
		if (src->c == 's' && (src->precision < str_len))
			ft_precision_cs(src);
	}
	ft_minwidth(src);
}
