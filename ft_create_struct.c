/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_struct.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/31 17:11:25 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/06 17:28:44 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

t_flags		*ft_create_struct(void)
{
	t_flags *new;

	new = (t_flags*)malloc(sizeof(t_flags));
	new->hesh = 0;
	new->minus = 0;
	new->zero = 0;
	new->plus = 0;
	new->space = 0;
	new->min_width = -1;
	new->precision = -1;
	new->modifier = ft_strnew(2);
	new->c = 0;
	new->prefix_str = NULL;
	new->s_str = NULL;
	new->n_bytes = 0;
	new->f = 0;
	return (new);
}

void		ft_clean_struct(t_flags *src)
{
	src->hesh = 0;
	src->minus = 0;
	src->zero = 0;
	src->plus = 0;
	src->space = 0;
	src->min_width = -1;
	src->precision = -1;
	src->c = 0;
	ft_bzero(src->modifier, 3);
	ft_strdel(&src->prefix_str);
	ft_strdel(&src->s_str);
	src->f = 0;
}
