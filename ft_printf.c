/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 13:47:45 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 13:48:03 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <wctype.h>

int		ft_printf(char *sv, ...)
{
	va_list		pc;
	size_t		i;
	t_flags		*new;

	new = ft_create_struct();
	va_start(pc, sv);
	while (*sv != '\0')
	{
		if (*sv == '%')
		{
			(*sv == '%' && *(sv + 1) == '\0') ? (ft_result(new, 0)) : 0;
			if (*(++sv) != '%')
			{
				if ((i = ft_get_flag(&pc, sv, new, 0)) == 0)
					return (new->n_bytes);
				sv = sv + i;
			}
			(*sv == '%' && *(sv - 1) == '%') ? (sv += ft_p_str(sv, new)) : 0;
		}
		else
			sv += ft_p_str(sv, new);
		(*sv == '\0') ? ft_result(new, 0) : 0;
	}
	va_end(pc);
	return (new->n_bytes);
}

void	ft_c_null(t_flags *src, char nul)
{
	if (src->prefix_str)
		ft_putstr(src->prefix_str);
	write(1, &nul, 1);
	ft_putstr(src->s_str);
	src->n_bytes += ft_strlen(src->prefix_str) + ft_strlen(src->s_str);
}

void	ft_result(t_flags *src, char nul)
{
	char		*tmp;
	size_t		len;

	if (src->c == 'c' && src->f == 1 && src->minus == 1 && src->min_width > 0)
	{
		ft_c_null(src, nul);
		return ;
	}
	if (!src->s_str && !src->prefix_str)
		return ;
	if (src->s_str && src->prefix_str)
		tmp = ft_strjoin(src->prefix_str, src->s_str);
	else if (src->s_str)
		tmp = src->s_str;
	else if (src->prefix_str)
		tmp = src->prefix_str;
	len = ft_strlen(tmp);
	if ((len + src->n_bytes) > INT_MAX)
		exit(-1);
	else
		ft_putstr(tmp);
	if (src->c == 'c' && src->f == 1)
		write(1, &nul, 1);
	src->n_bytes += len;
}
