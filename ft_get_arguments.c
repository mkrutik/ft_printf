/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_arguments.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 13:24:54 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 13:29:52 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_get_arguments_di(t_flags *src, va_list *pc)
{
	if (src->modifier[0] == 'h' && src->modifier[1] == 'h')
		src->s_str = ft_itoa_b_ssize_t((signed char)va_arg(*pc, int), 10);
	else if (src->modifier[0] == 'a' && src->modifier[1] != 'a')
		ft_di_a(src, va_arg(*pc, int*), va_arg(*pc, size_t));
	else if (src->modifier[0] == 'a' && src->modifier[1] == 'a')
		ft_di_aa(src, va_arg(*pc, int**), va_arg(*pc, size_t),
				va_arg(*pc, size_t));
	else if (src->modifier[0] == 'h' && src->modifier[1] != 'h')
		src->s_str = ft_itoa_b_ssize_t((short)va_arg(*pc, int), 10);
	else if (src->modifier[0] == 'l' && src->modifier[1] != 'l')
		src->s_str = ft_itoa_b_ssize_t(va_arg(*pc, long), 10);
	else if (src->modifier[0] == 'l' && src->modifier[1] == 'l')
		src->s_str = ft_itoa_b_ssize_t(va_arg(*pc, long long), 10);
	else if (src->modifier[0] == 'j')
		src->s_str = ft_itoa_b_ssize_t(va_arg(*pc, intmax_t), 10);
	else if (src->modifier[0] == 'z')
		src->s_str = ft_itoa_b_ssize_t(va_arg(*pc, ssize_t), 10);
	else
		src->s_str = ft_itoa_b_ssize_t(va_arg(*pc, int), 10);
}

void	ft_get_arguments_oux(t_flags *src, va_list *pc, ssize_t base)
{
	(src->c == 'o') ? base = 8 : 0;
	(src->c == 'x' || src->c == 'X') ? base = 16 : 0;
	if (src->modifier[0] == 'h' && src->modifier[1] == 'h')
		src->s_str = ft_itoa_b_size_t((unsigned char)va_arg(*pc, int), base);
	else if (src->modifier[0] == 'h' && src->modifier[1] != 'h')
		src->s_str = ft_itoa_b_size_t((unsigned short)va_arg(*pc, int), base);
	else if (src->modifier[0] == 'l' && src->modifier[1] != 'l')
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, unsigned long int), base);
	else if (src->modifier[0] == 'l' && src->modifier[1] == 'l')
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, long long), base);
	else if (src->modifier[0] == 'j')
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, intmax_t), base);
	else if (src->modifier[0] == 'z')
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, size_t), base);
	else
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, unsigned int), base);
	base = 0;
	if (src->c == 'X')
		while (src->s_str[base] != '\0')
		{
			(ft_isalpha(src->s_str[base])) ? (src->s_str[base] =
					ft_toupper(src->s_str[base])) : 0;
			base++;
		}
}

void	ft_get_arguments_cs(t_flags *src, va_list *pc, char *tmp)
{
	if (src->modifier[0] == 'l' && src->modifier[1] != 'l')
		ft_get_wchar_t(src, pc, 0);
	else if (src->modifier[0] == 'a' && src->modifier[1] == 'a' &&
			src->c == 's')
		ft_d_array(va_arg(*pc, char**), src);
	else
	{
		if (src->c != 'c')
		{
			tmp = va_arg(*pc, char*);
			src->s_str = (tmp != NULL) ? (ft_strdup(tmp)) :
				(ft_strdup("(null)"));
			return ;
		}
		src->s_str = ft_strnew(1);
		src->s_str[0] = (char)va_arg(*pc, int);
	}
	if (src->c == 'c')
		ft_if_c_nul(src);
}

int		ft_kostul(t_flags *src)
{
	if (((src->c == 'd' || src->c == 'o' || src->c == 'x' || src->c == 'u' ||
		src->c == 'i' || src->c == 'X') && src->precision == 0 && src->s_str[0]
		== '0') || (src->c == 's' && src->precision == 0
			&& src->min_width <= 0))
	{
		if (src->min_width > 0)
		{
			ft_bzero(src->s_str, ft_strlen(src->s_str));
			ft_minwidth_for_dioux_wizout_zero(src);
		}
		else if (src->hesh == 1 && src->c == 'o')
			src->s_str[0] = '0';
		else
			ft_bzero(src->s_str, ft_strlen(src->s_str));
		return (1);
	}
	else if (src->c == 'p' && src->s_str[0] == '0' && src->precision == 0)
	{
		ft_strdel(&src->s_str);
		src->s_str = ft_strdup("0x");
	}
	if ((src->c == 'c' && src->s_str[0] == '\0') || (src->c == 's' &&
				src->s_str[0] == '\0'))
		src->f = 1;
	return (0);
}

void	ft_modification_size(t_flags *src, va_list *pc)
{
	if ((src->c == 'c' || src->c == 's') && src->s_str == NULL)
		ft_get_arguments_cs(src, pc, NULL);
	else if (src->c == 'd' || src->c == 'i')
		ft_get_arguments_di(src, pc);
	else if (src->c == 'o' || src->c == 'u' || src->c == 'x' || src->c == 'X')
		ft_get_arguments_oux(src, pc, 10);
	else if (src->c == 'p')
		src->s_str = ft_itoa_b_size_t(va_arg((*pc), size_t), 16);
	else if (src->c == 'b')
		src->s_str = ft_itoa_b_size_t(va_arg(*pc, size_t), 2);
	else if (src->s_str[0] == '0' && (src->c == 'x' || src->c == 'X'))
		src->hesh = 0;
	if (ft_kostul(src) == 1)
	{
		ft_result(src, 0);
		return ;
	}
	if (src->precision == 0 && (src->c == 'o' || src->c == 'x' || src->c == 'd')
			&& src->s_str[0] == '0')
	{
		src->s_str = NULL;
		ft_result(src, 0);
		return ;
	}
	ft_do_precision(src);
}
