/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 17:27:16 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/06 17:27:53 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_check_size_modifier(char c)
{
	if (c == 'l' || c == 'L' || c == 'h' || c == 'j' || c == 'z' || c == 'a')
		return (1);
	else
		return (0);
}

int		ft_check_conversion_letter(char c)
{
	if (c == 'c' || c == 'C' || c == 'd' || c == 'D'
		|| c == 'i' || c == 'o' || c == 'O' || c == 'p' ||
		c == 's' || c == 'S' || c == 'u' || c == 'U' || c == 'x' ||
		c == 'X' || c == 'b')
		return (1);
	else
		return (0);
}

int		ft_is_specification(char c)
{
	if (c == 'c' || c == 'C' || c == 'd' || c == 'D'
		|| c == 'i' || c == 'o' || c == 'O' || c == 'p'
		|| c == 's' || c == 'S' || c == 'u' || c == 'U' || c == 'x'
		|| c == 'X' || c == 'l' || c == 'h'
		|| c == 'j' || c == 'z' || c == '+' || c == '-' || c == '0'
		|| c == '#' || c == ' ' || c == '.' || ft_isdigit(c) ||
		c == '*' || c == 'a')
		return (1);
	else
		return (0);
}
