/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_if_big_letter.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 17:40:56 by mkrutik           #+#    #+#             */
/*   Updated: 2017/01/31 19:21:42 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_if_big_letter(t_flags *src, char c)
{
	if (c == 'C' || c == 'S' || c == 'D' || c == 'O' || c == 'U')
	{
		src->c = ft_tolower(c);
		src->modifier[0] = 'l';
		src->modifier[1] = '\0';
	}
	else
		src->c = c;
}
