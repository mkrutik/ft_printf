/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_base_for_ssize_t.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/23 11:10:04 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/06 17:40:13 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int		ft_len_size_t(size_t value, size_t base)
{
	int len;

	len = 1;
	while (value >= base)
	{
		value = value / base;
		len++;
	}
	return (len);
}

char			*ft_itoa_b_ssize_t(ssize_t value, ssize_t base)
{
	char		*res;
	size_t		tmp;
	int			len;
	int			sign;

	if (base < 2 || base > 16)
		return (NULL);
	tmp = (value < 0) ? -value : value;
	sign = (value < 0 && base == 10) ? 1 : 0;
	len = ft_len_size_t(tmp, base) + sign;
	res = ft_strnew(len);
	len--;
	if (sign == 1)
		res[0] = '-';
	while (len >= sign)
	{
		res[len--] = ((tmp % base) > 9) ? ((tmp % base) + 'a' - 10) :
			((tmp % base) + '0');
		tmp = tmp / base;
	}
	return (res);
}

char			*ft_itoa_b_size_t(size_t value, ssize_t base)
{
	char		*res;
	int			len;
	size_t		base_t;

	if (base < 2 || base > 16)
		return (NULL);
	base_t = base;
	len = ft_len_size_t(value, base_t);
	res = ft_strnew(len);
	len--;
	while (len >= 0)
	{
		res[len--] = ((value % base_t) > 9) ? ((value % base_t) + 'a' - 10) :
			((value % base_t) + '0');
		value = value / base_t;
	}
	return (res);
}
