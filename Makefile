# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/01/31 15:26:25 by mkrutik           #+#    #+#              #
#    Updated: 2017/02/08 13:26:20 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC =	ft_create_struct.c \
		ft_get_arguments.c \
		ft_minwidth.c \
		ft_minwidth_2.c \
		ft_do_precision.c \
		ft_get_wchar_t.c \
		ft_printf.c \
		ft_find_prefix_str.c \
		ft_if_big_letter.c \
		ft_check.c \
		ft_find_specification.c \
		ft_itoa_base_for_ll.c \
		ft_bonus.c \
		ft_if_c_nul.c

OBJ = $(SRC:.c=.o)

LIBOBJ = libft/*.o

HEAD = -I ft_printf.h

CFLAGS = -c -Wall -Wextra -Werror

LIBINC = -I libft/libft.h -L./libft -lft

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	ar rc $(NAME) $(OBJ) $(LIBOBJ)
	ranlib $(NAME)

%.o: %.c
	gcc $(HEAD) $(CFLAGS) -o $@ $<

clean:
	rm -f $(OBJ)
	make clean -C libft/

fclean: clean
	rm -f $(NAME)
	make fclean -C libft/

re: fclean all
