/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_minwidth.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 12:01:22 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 12:34:15 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_minwidth_di_with_precision(t_flags *src)
{
	char *tmp;
	char *res;

	if (src->plus == 1 && src->s_str[0] != '-' && src->minus != 1)
	{
		tmp = "+";
		res = ft_strjoin(tmp, src->s_str);
		ft_strdel(&src->s_str);
		src->s_str = res;
	}
	ft_minwidth_for_dioux_wizout_zero(src);
	if (src->s_str[0] != '-' && src->s_str[0] != '+' && (src->plus == 1 ||
				src->space == 1) && src->minus == 1)
	{
		tmp = (src->plus == 1) ? "+" : " ";
		res = ft_strjoin(tmp, src->s_str);
		tmp = ft_strsub(res, 0, (ft_strlen(res) - 1));
		ft_strdel(&src->s_str);
		ft_strdel(&res);
		src->s_str = tmp;
	}
}

void	ft_minwidth_di(t_flags *src)
{
	char	*tmp;

	if (src->zero != 1 || src->precision >= 0 || src->minus == 1)
		ft_minwidth_di_with_precision(src);
	else if (src->precision < 0 && src->zero == 1 && src->minus != 1 &&
			src->min_width > (int)ft_strlen(src->s_str))
	{
		src->precision = src->min_width;
		ft_precision_dioux(src, 2);
		if (src->s_str[0] != '-' && (src->plus == 1 || src->space == 1))
			src->s_str[0] = (src->plus == 1) ? '+' : ' ';
	}
	else if (src->plus == 1)
	{
		tmp = ft_strjoin("+", src->s_str);
		ft_strdel(&src->s_str);
		src->s_str = tmp;
	}
}

void	ft_minwidth_ouxp(t_flags *src)
{
	if (src->zero != 1 || src->precision >= 0 || src->minus == 1)
	{
		if ((src->hesh == 1 && src->c != 'u') || src->c == 'p')
			ft_add_prefix_ox(src, 1, 0, NULL);
		ft_minwidth_for_dioux_wizout_zero(src);
	}
	else if (src->zero == 1 && src->precision < 0 && src->minus == 0)
	{
		ft_minwidth_for_oux_with_zero(src);
		if ((src->c != 'u' && src->hesh == 1))
			ft_add_prefix_ox(src, 2, 0, NULL);
		if (src->c == 'p' && src->s_str[ft_strlen(src->s_str) - 1] ==
				'0' && src->s_str[0] == '0')
			ft_add_prefix_ox(src, 2, 0, NULL);
		else if (src->c == 'p')
			ft_add_prefix_ox(src, 1, 0, NULL);
	}
}

void	ft_add_prefix_for_di(t_flags *src)
{
	char *tmp;
	char *res;

	tmp = (src->plus == 1) ? "+" : " ";
	res = ft_strjoin(tmp, src->s_str);
	ft_strdel(&src->s_str);
	src->s_str = res;
}

void	ft_minwidth(t_flags *s)
{
	if (s->min_width > 0)
	{
		if (s->c == 'd' || s->c == 'i')
			ft_minwidth_di(s);
		if ((s->c == 'o' || s->c == 'u' || s->c == 'x' ||
					s->c == 'X' || s->c == 'p'))
			ft_minwidth_ouxp(s);
		else if (s->c == 'c' || s->c == 's')
			ft_minwidth_for_cs(s, 0);
	}
	else if ((s->c == 'x' || s->c == 'X' || s->c == 'o')
			&& s->hesh == 1 && s->s_str[0] != '0')
		ft_add_prefix_ox(s, 1, 0, NULL);
	else if (s->c == 'x' && s->hesh == 1 && s->precision >= 0)
		ft_add_prefix_ox(s, 1, 0, NULL);
	else if (s->c == 'p' && s->precision != 0)
		ft_add_prefix_ox(s, 1, 0, NULL);
	if (s->min_width <= 0 && s->precision < 0 && (s->c == 'd' ||
		s->c == 'i') && (s->plus == 1 || s->space == 1) && s->s_str[0]
			!= '-')
		ft_add_prefix_for_di(s);
	ft_result(s, 0);
}
