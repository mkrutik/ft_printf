/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_prefix_str.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 17:32:00 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 10:35:22 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_insert(char **src, int x, int y, char *tmp)
{
	char	*res;
	char	*temp;

	res = ft_strsub(*src, 0, x);
	if (res)
		temp = ft_strjoin(res, tmp);
	else
		temp = ft_strdup(tmp);
	ft_strdel(&res);
	res = ft_strsub(*src, (y + 1), ft_strlen(*src) - y);
	ft_strdel(src);
	if (res)
		*src = ft_strjoin(temp, res);
	else
		*src = ft_strdup(temp);
	ft_strdel(&res);
	ft_strdel(&temp);
}

void	ft_do_color(t_flags *src, int x, int y)
{
	char *tmp;

	tmp = src->prefix_str;
	if (ft_strncmp(tmp + x, "{EOC}", 5) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0m");
	else if (ft_strncmp(tmp + x, "{BLACK}", 7) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;30m");
	else if (ft_strncmp(tmp + x, "{RED}", 5) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;31m");
	else if (ft_strncmp(tmp + x, "{GREEN}", 7) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;32m");
	else if (ft_strncmp(tmp + x, "{YELLOW}", 8) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;33m");
	else if (ft_strncmp(tmp + x, "{BLUE}", 6) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;34m");
	else if (ft_strncmp(tmp + x, "{MAGENTA}", 9) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;35m");
	else if (ft_strncmp(tmp + x, "{CYAN}", 6) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;36m");
	else if (ft_strncmp(tmp + x, "{GRAY}", 6) == 0)
		ft_insert(&src->prefix_str, x, y, "\033[0;37m");
}

void	ft_find_color(t_flags *src)
{
	int		i;
	int		start;
	int		end;

	start = -1;
	end = -1;
	i = 0;
	while (src->prefix_str[i])
	{
		if (src->prefix_str[i] == '{')
			start = i;
		if (src->prefix_str[i] == '}')
			end = i;
		if (end != -1 && start != -1)
		{
			ft_do_color(src, start, end);
			start = -1;
			end = -1;
		}
		i++;
	}
}

size_t	ft_p_str(char *str, t_flags *src)
{
	size_t		len;
	char		*res;

	len = 1;
	while (str[len] != '%' && str[len] != '\0')
		len++;
	if (src->prefix_str)
	{
		res = ft_strjoin(src->prefix_str, ft_strsub(str, 0, len));
		ft_strdel(&src->prefix_str);
		src->prefix_str = res;
	}
	else
		src->prefix_str = ft_strsub(str, 0, len);
	ft_find_color(src);
	return (len);
}
