/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_wchar_t.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 11:08:15 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 11:10:40 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_wchar_to_array(wchar_t c, char *point)
{
	if (c < (1 << 7))
	{
		*point++ = (unsigned char)(c);
	}
	else if (c < (1 << 11))
	{
		*point++ = (unsigned char)((c >> 6) | 0xC0);
		*point++ = (unsigned char)((c & 0x3F) | 0x80);
	}
	else if (c < (1 << 16))
	{
		*point++ = (unsigned char)(((c >> 12)) | 0xE0);
		*point++ = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		*point++ = (unsigned char)((c & 0x3F) | 0x80);
	}
	else if (c < (1 << 21))
	{
		*point++ = (unsigned char)(((c >> 18)) | 0xF0);
		*point++ = (unsigned char)(((c >> 12) & 0x3F) | 0x80);
		*point++ = (unsigned char)(((c >> 6) & 0x3F) | 0x80);
		*point++ = (unsigned char)((c & 0x3F) | 0x80);
	}
	*point = '\0';
}

void	ft_get_wint_t(t_flags *src, va_list *pc)
{
	wchar_t		c;
	char		res[5];

	if ((c = va_arg(*pc, wchar_t)) == L'\0')
	{
		src->s_str = ft_strnew(0);
		return ;
	}
	ft_wchar_to_array(c, res);
	src->s_str = ft_strdup(res);
}

void	ft_get_wchar_valid(t_flags *src, wchar_t *tmp, size_t i, char *res)
{
	char	*f_j;
	int		pres;

	pres = (src->precision < 0) ? 2147483647 : src->precision;
	while (tmp[i++])
	{
		ft_wchar_to_array(tmp[i - 1], res);
		if (pres >= 0 && pres >= (int)ft_strlen(res))
		{
			pres -= ft_strlen(res);
			if (src->s_str != NULL)
			{
				f_j = ft_strjoin(src->s_str, res);
				ft_strdel(&src->s_str);
				src->s_str = f_j;
			}
			else
				src->s_str = ft_strdup(res);
		}
	}
}

void	ft_get_wchar_t(t_flags *src, va_list *pc, size_t i)
{
	wchar_t		*tmp;
	char		res[5];

	if (src->c == 's')
	{
		tmp = va_arg(*pc, wchar_t*);
		if (tmp == NULL)
		{
			ft_strdel(&src->s_str);
			src->s_str = ft_strdup("(null)");
		}
		else if (tmp[i] == L'\0')
			src->s_str = ft_strnew(0);
		else
			ft_get_wchar_valid(src, tmp, i, res);
		if (src->s_str == NULL)
			src->s_str = ft_strnew(0);
		return ;
	}
	ft_get_wint_t(src, pc);
}
