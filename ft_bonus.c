/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bonus.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 12:56:04 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 12:56:50 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

size_t	ft_len_int(int src)
{
	size_t		len;
	unsigned	tmp;

	len = 1;
	if (src < 0)
	{
		len++;
		tmp = -src;
	}
	else
		tmp = src;
	while (tmp > 9)
	{
		len++;
		tmp /= 10;
	}
	return (len);
}

void	ft_line(size_t max, size_t len)
{
	size_t	i;

	i = 0;
	while (i <= ((len * max) + len))
	{
		write(1, "-", 1);
		i++;
	}
	write(1, "\n", 1);
}

void	ft_di_a(t_flags *src, int *tmp, size_t len)
{
	size_t		i;
	size_t		max;
	size_t		t;

	i = 0;
	max = 2;
	while (i < len)
	{
		if ((t = ft_len_int(tmp[i])) > max)
			max = t;
		i++;
	}
	i = 0;
	t = 0;
	max += 1;
	ft_line(max, len);
	while (i < len)
	{
		if (i == 0)
			write(1, "|", 1);
		t += ft_printf("%*d|", max, tmp[i]);
		i++;
	}
	write(1, "\n", 1);
	src->n_bytes += t;
}

void	ft_di_aa(t_flags *src, int **tmp, size_t line, size_t column)
{
	size_t i;

	i = 0;
	while (i < line)
	{
		ft_di_a(src, tmp[i], column);
		i++;
	}
	ft_clean_struct(src);
}

void	ft_d_array(char **array, t_flags *src)
{
	size_t	i;
	size_t	len;

	i = 0;
	len = 0;
	while (array[i] != '\0')
	{
		len += ft_printf("%s", array[i]);
		write(1, "\n", 1);
		i++;
	}
	src->n_bytes += len;
}
