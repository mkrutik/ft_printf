/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/16 15:01:11 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 13:49:56 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include "libft/libft.h"
# include <stdarg.h>
# include <stdio.h>
# include <limits.h>

typedef struct		s_flags
{
	int				hesh;
	int				minus;
	int				zero;
	int				plus;
	int				space;
	int				min_width;
	int				precision;
	char			*modifier;
	char			c;
	char			*prefix_str;
	char			*s_str;
	int				f;
	size_t			n_bytes;
}					t_flags;

void				ft_di_a(t_flags *src, int *tmp, size_t len);
void				ft_di_aa(t_flags *src, int **tmp, size_t line, size_t c);
int					ft_printf(char *sv, ...);
size_t				ft_p_str(char *str, t_flags *src);
int					ft_is_specification(char c);
int					ft_check_size_modifier(char c);
int					ft_check_conversion_letter(char c);
int					ft_get_flag(va_list *pc, char *sv, t_flags *src, int i);
int					ft_is_precision_or_m_width(char *sv, t_flags *src,
					va_list *pc, int i);
int					ft_is_letter(char *sv, t_flags *src);
int					ft_is_large_size(t_flags *src, char *sv);
int					ft_is_flag(char c, t_flags *src, va_list *pc);
void				ft_if_big_letter(t_flags *src, char c);
void				ft_print_struct(t_flags *str);
t_flags				*ft_create_struct(void);
void				ft_clean_struct(t_flags *src);
char				*ft_itoa_b_ssize_t(ssize_t value, ssize_t base);
char				*ft_itoa_b_size_t(size_t value, ssize_t base);
void				ft_get_arguments_di(t_flags *src, va_list *pc);
void				ft_get_arguments_oux(t_flags *src, va_list *pc, ssize_t b);
void				ft_get_arguments_cs(t_flags *src, va_list *pc, char *tmp);
void				ft_modification_size(t_flags *src, va_list *pc);
void				ft_get_wchar_t(t_flags *src, va_list *pc, size_t i);
void				ft_do_precision(t_flags *src);
void				ft_precision_dioux(t_flags *src, int i);
void				ft_precision_cs(t_flags *src);
void				ft_minwidth(t_flags *src);
void				ft_result(t_flags *src, char c);
void				ft_minwidth_for_dioux_wizout_zero(t_flags *src);
void				ft_minwidth_for_oux_with_zero(t_flags *src);
void				ft_add_prefix_ox(t_flags *src, int f, int i, char *res);
void				ft_minwidth_for_cs(t_flags *src, int i);
void				ft_d_array(char **array, t_flags *src);
void				ft_if_c_nul(t_flags *src);
#endif
