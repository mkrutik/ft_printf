/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_if_c_nul.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/08 13:21:43 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/08 13:30:34 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	ft_if_c_nul(t_flags *src)
{
	if (src->s_str[0] == '\0' && src->min_width <= 0)
		src->n_bytes += 1;
	else if (src->s_str[0] == '\0' && src->zero == 1 && src->min_width > 0)
		src->f = 1;
	else if (src->s_str[0] == '\0' && src->precision == 0)
	{
		src->f = 1;
		src->n_bytes++;
	}
}
