/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_specification.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mkrutik <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/06 17:33:47 by mkrutik           #+#    #+#             */
/*   Updated: 2017/02/06 17:53:15 by mkrutik          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int		ft_is_flag(char c, t_flags *src, va_list *pc)
{
	if (c == '-' || c == '+' || c == '0' || c == '#' || c == ' ' || c == '*')
	{
		src->minus = (c == '-') ? 1 : src->minus;
		src->zero = (c == '0') ? 1 : src->zero;
		src->plus = (c == '+') ? 1 : src->plus;
		src->space = (c == ' ') ? 1 : src->space;
		src->hesh = (c == '#') ? 1 : src->hesh;
		src->min_width = (c == '*') ? va_arg(*pc, int) : src->min_width;
		if (src->min_width < 0 && c == '*')
		{
			src->min_width *= -1;
			src->minus = 1;
		}
		return (1);
	}
	else
		return (0);
}

int		ft_is_large_size(t_flags *src, char *sv)
{
	if (sv[0] == 'j')
		if (src->modifier[0] < 'n')
			return (1);
	if (src->modifier[0] == 'j')
	{
		if (sv[0] > 'n')
			return (1);
		else
			return (0);
	}
	if (sv[0] > src->modifier[0])
		return (1);
	if (sv[0] == 'h' && sv[1] != 'h' && src->modifier[0] == 'h')
		return (1);
	if (sv[0] == 'l' && sv[1] == 'l' && src->modifier[0] == 'l')
		return (1);
	else
		return (0);
}

int		ft_is_letter(char *sv, t_flags *src)
{
	if (ft_check_size_modifier(sv[0]))
	{
		if (ft_is_large_size(src, sv))
		{
			ft_bzero(src->modifier, 3);
			if (sv[0] == sv[1])
			{
				src->modifier[0] = sv[0];
				src->modifier[1] = sv[0];
				return (2);
			}
			src->modifier[0] = sv[0];
			return (1);
		}
		if (sv[0] == sv[1])
			return (2);
		return (1);
	}
	else if (ft_check_conversion_letter(sv[0]))
	{
		ft_if_big_letter(src, sv[0]);
		return (1);
	}
	return (0);
}

int		ft_is_precision_or_m_width(char *sv, t_flags *src, va_list *pc, int i)
{
	size_t		tmp;

	tmp = 0;
	if (sv[i] == '.' && !(ft_isdigit(sv[i + 1])) && sv[i + 1] != '*')
	{
		src->precision = 0;
		return (1);
	}
	if (sv[i] == '.')
	{
		if (sv[++i] == '*')
		{
			src->precision = va_arg(*pc, int);
			return (2);
		}
		while (ft_isdigit(sv[i]))
			tmp = tmp * 10 + (sv[i++] - '0');
		src->precision = tmp;
		return (i);
	}
	while (ft_isdigit(sv[i]))
		tmp = tmp * 10 + (sv[i++] - '0');
	src->min_width = tmp;
	return (i);
}

int		ft_get_flag(va_list *pc, char *sv, t_flags *src, int i)
{
	while (1)
	{
		if (ft_is_flag(sv[i], src, pc))
			i++;
		if (ft_isdigit(sv[i]) && sv[i] != '0')
			i += ft_is_precision_or_m_width(sv + i, src, pc, 0);
		if (sv[i] == '.')
			i += ft_is_precision_or_m_width(sv + i, src, pc, 0);
		if (ft_isalpha(sv[i]))
			i += ft_is_letter(sv + i, src);
		if (src->c != '\0')
			break ;
		if (sv[i] == '\0')
			return (0);
		if (!(ft_is_specification(sv[i])))
		{
			src->c = 'c';
			src->s_str = ft_strnew(1);
			src->s_str[0] = sv[i++];
			break ;
		}
	}
	ft_modification_size(src, pc);
	ft_clean_struct(src);
	return (i);
}
